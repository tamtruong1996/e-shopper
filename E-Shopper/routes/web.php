<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\AddCountryController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\AddBlogController;
use App\Http\Controllers\Admin\EditBlogController;
use App\Http\Controllers\frontend\IndexController;
use App\Http\Controllers\frontend\LoginController;
use App\Http\Controllers\frontend\RegisterController;
use App\Http\Controllers\frontend\BlogSingleController;
use App\Http\Controllers\frontend\AccountController;
use App\Http\Controllers\frontend\ProductController;
use App\Http\Controllers\frontend\AddProductController;
use App\Http\Controllers\frontend\MailController;
use App\Http\Controllers\frontend\SearchController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware' => 'memberNotLogin'
], function () {
    Route::get('/member/index',[IndexController::class,'index'])->name('index');
    Route::get('/member/login',[LoginController::class,'index'])->name('login');
    Route::post('/member/login',[LoginController::class,'login'])->name('login');
    Route::get('/member/account/logout',[LoginController::class,'logout']);
    Route::get('/member/register',[RegisterController::class,'index'])->name('register');
    Route::post('/member/register',[RegisterController::class,'register'])->name('register');
});

Route::group(['middleware' => 'member'], function () {
    Route::get('/member/blog_list',[BlogSingleController::class,'index'])->name('bloglist');
    Route::get('/member/blog_single/{id}',[BlogSingleController::class,'showlist'])->name('blogsingle');
    Route::get('/ajax/blog_single',[BlogSingleController::class,'ajax_rate']);
    Route::post('/blog/comment/{id}',[BlogSingleController::class,'comment']);
    Route::get('/member/account/update',[AccountController::class,'index'])->name('account');
    Route::post('/member/account/update',[AccountController::class,'update_account']);
    Route::get('/member/account/myproduct',[ProductController::class,'index']);
    Route::get('/member/account/addproduct/{id}',[ProductController::class,'get_product']);
    Route::post('/member/account/addproduct/{id}',[ProductController::class,'add_product']);
    Route::get('/member/account/myproduct/edit/{id}',[ProductController::class,'get_update_product']);
    Route::post('/member/account/myproduct/edit/{id}',[ProductController::class,'post_update_product']);
    Route::get('/member/account/myproduct/delete/{id}',[ProductController::class,'delete_product']);
    Route::get('/member/account/product-details/{id}',[ProductController::class,'get_product_detail']);
    Route::post('/member/account/ajax.cart',[ProductController::class,'addtocart']);
    Route::get('/member/account/cart',[ProductController::class,'get_cart']);
    Route::post('/member/account/cart',[ProductController::class,'ajax_cart']);
    Route::get('/member/account/cart/checkout',[ProductController::class,'checkout']);
    Route::post('/member/account/cart/checkout',[ProductController::class,'check_sendmail']);
    Route::get('/member/account/cart/sendmail',[MailController::class,'sendmail']);
    Route::get('/member/account/cart/search',[SearchController::class,'get_search']);
    Route::get('/member/account/cart/search_advanced',[SearchController::class,'search_advanced']);
    Route::post('/member/index',[SearchController::class,'search_price']);

});


Auth::routes();
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function()
{
    Route::get('/admin/dashboard',[DashboardController::class, 'index'])->name('dashboard');
    Route::get('/admin/pages-profile',[UserController::class, 'index'])->name('pages-profile');
    Route::post('/admin/pages-profile',[UserController::class, 'update'])->name('pages-profile');
    Route::get('/admin/country',[CountryController::class, 'index'])->name('country');
    Route::get('/admin/addcountry',[AddCountryController::class, 'index'])->name('addcountry');
    Route::post('/admin/addcountry',[AddCountryController::class, 'add'])->name('addcountry');
    Route::get('/admin/delete/{id}',[CountryController::class, 'delete'])->name('delete-country');
    Route::get('/admin/blog',[BlogController::class, 'index'])->name('blog');
    Route::get('/admin/addblog',[AddBlogController::class, 'index'])->name('addblog');
    Route::post('/admin/addblog',[AddBlogController::class, 'add'])->name('addblog');
    Route::get('/admin/edit_blog/{id}',[EditBlogController::class, 'edit'])->name('editblog');
    Route::post('/admin/edit_blog/{id}',[EditBlogController::class, 'update'])->name('editblog');
    Route::get('/admin/delete_blog/{id}',[BlogController::class, 'delete'])->name('delete-blog');
});







