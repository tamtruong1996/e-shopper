<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
</head><!--/head-->
<body>
<section id="cart_items">
    <div class="container">
        <div style='margin-bottom:50px'>
            <h3>Thông tin đơn hàng cuả bạn :</h3>    
        </div>
        <div>
            <table>
                <thead>
                    <tr class="success">
                        <td class="description">Name</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['body'] as $val)
                        <tr>
                            <td class="cart_description">
                                <h4>{{$val['name']}}</h4>
                            </td>
                            <td class="cart_price">
                                <p>{{$val['price']}}</p>
                            </td>
                            <td class="cart_quantity">
                            <p>{{$val['qty']}}</p>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">${{$val['price']*$val['qty']}}</p>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">
                            <table class="table table-condensed total-result">
                                <tr>
                                    <td>Cart Sub Total</td>
                                    <td>${{$data['total']}}</td>
                                </tr>
                                <tr>
                                    <td>Exo Tax</td>
                                    <td>10%</td>
                                </tr>
                                <tr class="shipping-cost">
                                    <td>Shipping Cost</td>
                                    <td>Free</td>										
                                </tr>
                            </table>
                        </td>
                        <td colspan="3">
                            <h3>Total: ${{$data['total'] + $data['total']}}</h3>	
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
</body>
</html>