@extends('frontend.layouts.app')
@section('content')
<div class="col-sm-9">
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        @foreach($data as $val)
            <div class="single-blog-post">
                <h3>{{$val->title}}</h3>
                <div class="post-meta">
                    <ul>
                        <li><i class="fa fa-user"></i> {{$val->name}}</li>
                        <li><i class="fa fa-clock-o"></i>{{$val->created_at->toTimeString()}}</li>
                        <li><i class="fa fa-calendar"></i>{{$val->created_at->toFormattedDateString()}}</li>    
                    </ul>
                    <span>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                    </span>
                </div>
                <a href="/member/blog_single/{{$val->id}}">
                    <img src="/frontend/images/blog/{{$val->image}}" alt="" >
                </a>
                <p>{{$val->description}}</p>
                <a  class="btn btn-primary" href="/member/blog_single/{{$val->id}}">Read More</a>
            </div>
        @endforeach
        <div class="pagination-area">
        {{ $data->links(); }}
            <!-- <ul class="pagination">
                <li><a href="" class="active">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
            </ul> -->
        </div>
    </div>
</div>
@endsection