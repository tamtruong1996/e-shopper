@extends('frontend.layouts.app')
@section('content')

<section>
		<div class="container">
            @foreach($data as $val)
			<div class="row">
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$val->title}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i>{{$val->name}}</li>
									<li><i class="fa fa-clock-o"></i> {{$val->created_at->toTimeString()}}</li>
									<li><i class="fa fa-calendar"></i> {{$val->created_at->toFormattedDateString()}}</li>
								</ul>
								<!-- <span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span> -->
							</div>
							<a href="">
								<img src="/frontend/images/blog/{{$val->image}}" alt="">
							</a>
							<p>
							{{$val->txtContent}}
							</p>
							<div class="pager-area">
								<ul class="pager pull-right">
                                <!-- @if($previous)
									<li><a href="{{ URL::to( 'member/blog_single/'.$previous)}}">Pre</a></li>
                                @endif
                                @if($next)
									<li><a href="{{ URL::to( 'member/blog_single/'.$next)}}">Next</a></li>
                                @endif -->
                                @if($previous)
									<a href="{{ URL::to( 'member/blog_single/' . $previous ) }}">Previous</a>	
									@endif
									@if($next)
									<a href="{{ URL::to( 'member/blog_single/' . $next ) }}">Next</a>
									@endif
								</ul>
							</div>
						</div>
                        
					</div><!--/blog-post-area-->
					@endforeach

					<div class="rating-area">
						<div class="rate">
							<div class="vote">
								@for($i=1; $i<=5; $i++)
								<div class="star_{{$i}} ratings_stars {{ $i <= $tbc ? 'ratings_over' : '' }} "><input value="{{$i}}" type="hidden"></div>
								@endfor
								<span class="rate-np">{{$tbc}}</span>
							</div> 
						</div>
						
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->
					<div class="socials-share">
						<a href=""><img src="images/blog/socials.png" alt=""></a>
					</div><!--/socials-share-->

					<!-- <div class="media commnets">
						<a class="pull-left" href="#">
							<img class="media-object" src="images/blog/man-one.jpg" alt="">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Annie Davis</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<div class="blog-socials">
								<ul>
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-dribbble"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
								<a class="btn btn-primary" href="">Other Posts</a>
							</div>
						</div>
					</div> --><!--Comments-->
					
					<div class="response-area">
						<h2>3 RESPONSES</h2>
						<ul class="media-list">
						@foreach($comment as $val)
							@if($val['level']=0)
							<li class="media">
								<a class="pull-left" href="#">
									<img class="media-object" src="images/blog/man-two.jpg" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>{{$val->name}}</li>
										<li><i class="fa fa-clock-o"></i>{{$val->created_at->toTimeString()}}</li>
										<li><i class="fa fa-calendar"></i>{{$val->created_at->toTimeString()}}</li>
									</ul>
									<p>{{$val->cmt}}</p>
									<a class="btn btn-primary" id="{{$val->id_user}}"><i class="fa fa-reply"></i>Replay</a><br><br>
								</div>
							</li>
							@elseif($val['level']=$val['id_user'])
								@foreach($comment as $val)
									<li class="media second-media">
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$val->name}}</li>
												<li><i class="fa fa-clock-o"></i>{{$val->created_at->toTimeString()}}</li>
												<li><i class="fa fa-calendar"></i>{{$val->created_at->toTimeString()}}</li>
											</ul>
											<p>{{$val->cmt}}</p>
										</div>
									</li>
								@endforeach
							@endif
							@endforeach	
						</ul>	
									
					</div><!--/Response-area-->

					<div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<h2>Leave a replay</h2>
								<form method="post" enctype="multipart/form-data" action="{{ URL('/blog/comment/'.$val->id_blog)}}">
								@csrf
									<div class="text-area">
										<div class="blank-arrow">
											<label>Your Name</label>
										</div>
										<span>*</span>
										<input id="reply" type="hidden" value=0 name="level">
										<textarea name="message" rows="11"></textarea>
										<button type="submit" class="btn btn-primary">post comment</button>
									</div>
								</form>
							</div>
						</div>
					</div><!--/Repaly Box-->
				</div>	
			</div>
		</div>
	</section>
	<script>
	$(document).ready(function(){
		//vote
		$('.ratings_stars').hover(
			// Handles the mouseover
			function() {
				$(this).prevAll().andSelf().addClass('ratings_hover');
				// $(this).nextAll().removeClass('ratings_vote'); 
			},
			function() {
				$(this).prevAll().andSelf().removeClass('ratings_hover');
				// set_votes($(this).parent());
			}
		);
		$('.ratings_stars').click(function(){
			if ($(this).hasClass('ratings_over')) {
				$('.ratings_stars').removeClass('ratings_over');
				$(this).prevAll().andSelf().addClass('ratings_over');
			} else {
				$(this).prevAll().andSelf().addClass('ratings_over');
			}
			var values = $(this).find("input").val();
			var check = "{{Auth::check()}}";
			var id_blog = "{{$val['id']}}";
			if (check==true){
				$.ajax({
					type: 'get',
					url: "{{ URL('/ajax/blog_single') }}",
					data: {
						values: values,
						id_blog:id_blog,
					},
					success:function(data){
						
						// console.log(data);
					}
				});
			}else{
				alert('Vui lòng login để đánh giá');
			}
		});
	});
	$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });

	// check login comment va nhap du lieu comment
	$(document).ready(function(){
		$('form').submit(function(){
			var check_comment = "{{Auth::check()}}";
			var values_comment = $(this).find("textarea").val();
			if(check_comment==true){
				if(values_comment == ''){
					alert('Vui long nhap noi dung comment');
				}
				else{
					return true;
				}
			}
			else{
				alert('Vui long dang nhap de comment');
			}
			return false;
		});
	});

	$(document).ready(function(){
		$('a').click(function(){
			$reply = $(this).attr('id')
			// console.log($reply)
			$('input#reply').attr('value',$reply)
		});
	});
	
</script>
@endsection