@extends('frontend.layouts.app1')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="left-sidebar">
                <h2>PRODUCT</h2>
                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                    ACCOUNT
                                </a>
                            </h4>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-parent="#accordian" href="">
                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                    MY PRODUCT
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 padding-right">
            <div class="features_items">
                <section id="cart_items">
                    @if($product)
                        <div class="table-responsive cart_info">
                            <table id="list_product" class="table table-condensed" >
                                <thead>
                                    <tr class="cart_menu" >
                                        <td class="id">ID</td>
                                        <td class="description">Title</td>
                                        <td class="image">Image</td>
                                        <td class="price">Price</td>
                                        <td class="action">Action</td>
                                        <td class="delete"></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($product as $val)
                                        <tr>
                                            <td>{{$val['id']}}</td>
                                            <td>{{$val['name']}}</td>
                                            <td>
                                                <?php
                                                    $getArrImage = json_decode($val['hinhanh'], true);
                                                ?>
                                                <div class='col-sm-2'>
                                                    <div >
                                                        <center><img style="width:200%" src="/upload/product/{{$getArrImage[0]}}" alt=""></center>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{$val['price']}}</td>
                                            <td><a href="myproduct/edit/{{$val['id']}}"> Edit</a></td>
                                            <td><a href="myproduct/delete/{{$val['id']}}"> Delete</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            {{('Khong co san pham nao, vui long add them san pham')}}
                        @endif  
                        </div>
                    <a href="/member/account/addproduct/{{Auth::id()}}"><button class="btn btn-primary">Add new</button></a>
                </section>	
            </div>  
        </div>
    </div>
</div>
@endsection