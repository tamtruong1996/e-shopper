@extends('frontend.layouts.app1')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="left-sidebar">
                <h2>PRODUCT</h2>
                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                    ACCOUNT
                                </a>
                            </h4>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-parent="#accordian" href="/member/account/myproduct">
                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                    MY PRODUCT
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div>
            <div class="bill-to">
                <div class="form-one">
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                        <p>Update Product</p>
                            <input name="title" type="text" placeholder="Name" value="{{$product['name']}}">
                            <input name="price" type="text" placeholder="Price" value="{{$product['price']}}">
                            <select name="id_category">
                                @foreach($category as $val)
                                    <option value="{{$val->id}}" @selected($val->id == $product['id_category'])> {{$val->category}}</option>
                                @endforeach
                            </select><br><br>
                            <select name="id_brands">      
                                @foreach($brands as $val)
                                <option value="{{$val->id}}" @selected( $val->id == $product['id_brands'])>{{$val->brands}}</option>
                                @endforeach
                            </select><br><br>
                            <!-- <select name="status" placeholder='Sale'>            
                                <option value="0">New</option>
                                <option value="1" @selected($product['status']==1)> Sale</option>
                            </select><br><br> -->
                            <select name="status" id="status" placeholder='Sale' style='height:40px'>          
                                <option value="0" class='status_new'>New</option>
                                <option value="1" class='status_sale' @selected($val['status']==1)> Sale</option>
                            </select><br><br>
                            <input type="text"  class='sale_unit'  name="sale"  value="{{$val['sale']}}" style="width:30%;margin-bottom:0px;{{$val['status']==1 ?  '' : 'display: none'}}" /> 
                            <span class='sale_unit' style="{{$val['status']==1 ?  '' : 'display: none'}}">%</span> 
                            <p class="sale_unit" style="{{$val['status']==1 ?  '' : 'display: none'}}">{{$errors->first('sale')}}</p>
                            <input name="sale" placeholder="Sale (%)" value="{{$product['sale']}}">
                            
                            <input name="company" type="text" placeholder="Company Profile" value="{{$product['company']}}">
                            <label>Images</label>
                            <input type="file" name="files[]" multiple> 
                            <?php
                                $getArrImage = json_decode($product['hinhanh'], true);
                            ?>
                            @foreach($getArrImage as $key)
                            <div class='col-sm-2'>
                                <div >
                                    <center><img src="/upload/product/{{$key}}" alt="" style="width:50px; height:50px"></center>
                                </div>
                                <div>
                                    <center><input type="checkbox" name="delete[]" value="{{$key}}"></center>
                                </div>
                            </div>
                            @endforeach
                            <textarea name="detail">{{$product['detail']}}</textarea>
                            <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection