@extends('frontend.layouts.app1')
@section('content')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @if(!empty($data))
                    @foreach($data as $val)
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="/upload/product/IMG_85{{json_decode($val['hinhanh'],true)[0]}}" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{$val['name']}}</a></h4>
                            <p class="id_product">Web ID: {{$val['id']}}</p>
                        </td>
                        <td class="cart_price">
                            <p id="price">${{$val['price']}}</p>
                        </td>
                    
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a id="update_product" class="cart_quantity_up" href=""> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="{{$val['qty']}}" autocomplete="off" size="2">
                                <a id="update_product" class="cart_quantity_down" href=""> - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">${{$val['price']*$val['qty']}}</p>
                        </td>
                        <td class="cart_delete">
                            <a id="update_product" class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    @endforeach   
                @endif
                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Use Coupon Code</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Use Gift Voucher</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Estimate Shipping & Taxes</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Country:</label>
                            <select>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            
                        </li>
                        <li class="single_field">
                            <label>Region / State:</label>
                            <select>
                                <option>Select</option>
                                <option>Dhaka</option>
                                <option>London</option>
                                <option>Dillih</option>
                                <option>Lahore</option>
                                <option>Alaska</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                        
                        </li>
                        <li class="single_field zip-field">
                            <label>Zip Code:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Get Quotes</a>
                    <a class="btn btn-default check_out" href="">Continue</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Cart Sub Total <span class="cart_sub_total">${{$cart_sub_total}}</span></li>
                        <li>Eco Tax <span class="eco_tax">$2</span></li>
                        <li>Shipping Cost <span class="shipping_cost">Free</span></li>
                        <li>Total <span class="total_all">${{$cart_sub_total}}</span></li>
                    </ul>
                        <a class="btn btn-default update" href="">Update</a>
                        <a class="btn btn-default check_out" href="/member/account/cart/checkout">Check Out</a>
                </div>  
            </div>
        </div>
    </div>
</section><!--/#do_action-->
<script>
    $(document).ready(function() {
        $('span.total-cart').text('{{$sum}}');
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $("a#update_product").click(function(e){
        e.preventDefault();        
        $id = ($(this).closest('tr').find('p.id_product').text()).slice(8);
        // console.log($id);
        $ten = $(this).attr('class');
        // console.log($ten);
        $qty = Number($(this).closest('tr').find('input').val());
        // console.log($qty);
        $qty_cart = Number($('span.total-cart').text());
        // console.log($qty_cart);
        $price = Number(($(this).closest('tr').find('p#price').text()).slice(1));
        // console.log($price);
        $cart_total = Number(($(this).closest('tr').find('p.cart_total_price').text()).slice(1));
        // console.log($cart_total);
        $cart_sub_total = Number(($('.cart_sub_total').text()).slice(1));
        // console.log($cart_sub_total);

        if($ten == 'cart_quantity_up'){
            $qty = $qty + 1;
            $qty_cart = $qty_cart + 1;
            $cart_total = $cart_total + $price;
            $cart_sub_total = $cart_sub_total + $price;
        }
        if($ten == 'cart_quantity_down'){
            $qty_cart = $qty_cart - 1;
            if($qty>1){
                $qty = $qty - 1;
                $cart_total = $cart_total - $price;
                $cart_sub_total = $cart_sub_total - $price;
            }else{
                $qty = 0;
                $cart_sub_total = $cart_sub_total - $cart_total;
                $(this).closest('tr').hide();
            }
        }
        if($ten == 'cart_quantity_delete'){
            $qty_cart = $qty_cart - $qty;
            $qty = 0;
            $cart_sub_total = $cart_sub_total - $cart_total;
            $(this).closest('tr').hide();
        }
        $(this).closest('tr').find('input').val($qty);
        $('span.total-cart').text($qty_cart);
        $(this).closest('tr').find('p.cart_total_price').text('$'+$cart_total);
        $('.cart_sub_total').text('$'+$cart_sub_total);
        $('.total_all').text('$'+$cart_sub_total);

        $.ajax({
            type:'POST',
            url:'/member/account/cart',
            data:{
                id:$id,
                qty:$qty,
                _token:"{{ csrf_token() }}",
            },
            success:function($qty){
                console.log($qty);
           },
        });
	});

</script>

@endsection