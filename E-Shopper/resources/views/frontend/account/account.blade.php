@extends('frontend.layouts.app1')
@section('content')
<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>ACCOUNT</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											ACCOUNT
										</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-parent="#accordian" href="/member/account/myproduct">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>	
											MY PRODUCT
										</a>
									</h4>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="bill-to">
					@if(session('success')) 
						<div class="alert alert-success alert-dismissible"> 
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<h4><i class="icon fa fa-check"></i> Thông báo!</h4>
							{{session('success')}} 
						</div> 
					@endif

					@if($errors->any())
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<h4><i class="icon fa fa-check"></i> Thông báo!</h4> 
							<ul> 
								@foreach($errors->all() as $error) 
								<li>{{$error}}</li> 
								@endforeach 
							</ul> 
						</div> 
					@endif
						<div class="form-one">
							<form method="post" enctype="multipart/form-data">
								@csrf
								<p>User Update</p>
								<input disabled="disabled" name="id" type="text" placeholder="ID" value="{{Auth::id()}}">
								<input name="name" type="text" placeholder="Name" value="{{Auth::user()->name}}">
								<input name="email" type="text" placeholder="Email" value="{{Auth::user()->email}}">
								<input name="password" type="password" placeholder="Password" value="{{Auth::user()->password}}">
								<input name="phone" type="text" placeholder="Phone" value="{{Auth::user()->phone}}">
								<img style ="width: 200px; height: 150px" src="{{url('upload/member/avatar/')}}/{{Auth::user()->avatar}}">
								<input name="avatar" type="file" placeholder="Avatar" value="{{Auth::user()->avatar}}">	
								<button type="submit" class="btn btn-primary" name="Save">Save</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection