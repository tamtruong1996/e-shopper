@extends('frontend.layouts.app1')
@section('content')
<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>PRODUCT</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											ACCOUNT
										</a>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-parent="#accordian" href="/member/account/myproduct">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											MY PRODUCT
										</a>
									</h4>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				<div>
					<div class="bill-to">
						<div class="form-one">
							<form method="post" enctype="multipart/form-data">
								@csrf
								<p>Create Product</p>
								<input name="title" type="text" placeholder="Name" value="">
								<input name="price" type="text" placeholder="Price" value="">
								<div>
									<label>Category</label>
									<select name="id_category">
										@foreach ($category as $val)
											<option value="{{$val['id']}}">{{$val['category']}}</option>	
										@endforeach
									</select>
								</div><br>
								<div>
									<label>Brand</label>
									<select name="id_brands">
										@foreach ($brands as $val)
											<option value="{{$val['id']}}">{{$val['brands']}}</option>	
										@endforeach
									</select>
								</div><br>
								<div>
									<label>Sale</label>
									<select class="form-control form-control-line" name="status">
										<option value="0">New</option>
										<option value="1">Sale</option>
									</select>
								</div><br>
								<input style="width:50%" id="check_sale" name="sale" type="hidden" placeholder="Sale (%)" value="">
								<input name="company" type="text" placeholder="Company Profile" value="">
								<label>Images</label>
								<input type="file" name="files[]" multiple>
								<textarea name="detail" placeholder="Detail"></textarea>
								<button type="submit" class="btn btn-primary">Create</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script>
		$(document).ready(function(){
			$('select').click(function(){
				var sale = $(this).val();	
				if(sale==1){
					$('input#check_sale').attr('type','text');
					$('input#check_sale').show();
				}
				else{
					$('input#check_sale').hide();
				}
			});
		});
	</script>
@endsection