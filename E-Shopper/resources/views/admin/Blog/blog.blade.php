@extends('admin.layouts.app')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Blog</h4>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex align-items-center justify-content-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Blog</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive-sm">
                    <table class="table">
                        <thead>
                            <tr>    
                                <th scope="col">ID</th>
                                <th scope="col">Title</th>
                                <th scope="col">Image</th>
                                <th scope="col">Description</th>
                                <th scope="col">Content</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data_blog as $val)
                                <tr>
                                    <td>{{$val['id']}}</td>
                                    <td>{{$val['title']}}</td>
                                    <td>{{$val['image']}}</td>
                                    <td>{{$val['description']}}</td>
                                    <td>{{$val['txtContent']}}</td>
                                    <td><a href="edit_blog/{{$val['id']}}"> Edit</a><br><a href="delete_blog/{{$val['id']}}"> Delete</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pTrang">
                    <a href="#">></a>
                    <a href="#">2</a>
                    <a class="active" href="#">1</a>
                    <a href="#"><</a>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group" >
        <div class="col-sm-12">
            <a href="{{ route('addblog') }}"><button class="btn btn-success">Add Blog</button></a>
        </div>
    </div>
</div>
@endsection