@extends('admin.layouts.app')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Edit Blog</h4>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex align-items-center justify-content-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Blog</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if(session('success')) 
        <div class="alert alert-success alert-dismissible"> 
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
            {{session('success')}} 
        </div> 
    @endif
    @if($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
		@endif
    <div class="row">
        <div class="col-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-30" method="post" enctype="multipart/form-data">
                    @csrf
                    @foreach ($data_edit_blog as $val)
                        <div class="form-group">
                            <label>Title(*)</label>
                            <input type="text" class="form-control" name="title" value="{{$val['title']}}">
                        </div>
                        <div class="form-group">
                                <label>Image</label>
                                <div class="col-md-12">
                                    <img src="/upload/blog/image/{{$val['image']}}" class="rounded-circle" width="150" />
                                </div>
                                <div class="col-md-12">
                                    <input type="file" name="image"/>
                                </div>
                            </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" class="form-control" name="description" value="{{$val['description']}}">
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea name="txtContent" class="form-control" id="demo" cols="30" rows="10">{{$val['txtContent']}}</textarea>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success">Edit Blog</button>
                            </div>
                        </div>
                    @endforeach
                </form>
            </div>
        </div>
    </div>
</div>
@endsection 