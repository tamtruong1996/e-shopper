<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    use HasFactory;
    protected $table = 'blog';
    protected $fillable = [
        'id',
        'title',
        'image',
        'txtContent',
        'description',
        'created_at',
    ];
    public $timestamps = true;
}
