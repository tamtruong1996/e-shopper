<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    use HasFactory;
    protected $table = 'comment';
    protected $fillable = [
        'id',
        'cmt',
        'id_user',
        'id_blog',
        'avatar',
        'name',
        'level',
        'created_at',
    ];
    public $timestamps = true;
}
