<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    use HasFactory;
    protected $table = 'product';
    protected $fillable = [
        'id',
        'name',
        'price',
        'id_category',
        'id_user',
        'id_brand',
        'status',
        'sale',
        'company',
        'hinhanh',
        'detail',
    ];
    public $timestamps = true;
}
