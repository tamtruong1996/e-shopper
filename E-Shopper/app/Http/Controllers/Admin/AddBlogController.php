<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\blog;
use App\Http\Requests\AddBlogRequest;

class AddBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('.admin.Blog.addblog');
    }

    public function add(AddBlogRequest $request)
    {
        $blog = new blog();
        // $blog->title="$request->title";

        $data = request()->except(['_token']);
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['txtContent'] = $request->txtContent;
        $file = $request->image;  
        if(!empty($file)){
            $data['image']=$file->getClientOriginalName();
        }
        
        if($blog->insert($data)){
            if(!empty($file)){
                $file->move('upload/blog/image',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',__('Add profile success.'));
        }
        else{
            return redirect()->back()->withErrors('Add profile error.');
        }
        // $blog->description="$request->description";
        // $blog->txtContent="$request->txtContent";
        $blog->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
