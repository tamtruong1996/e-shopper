<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\blog;
use App\Http\Requests\AddBlogRequest;

class EditBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_edit_blog = blog::where('id',$id)->get()->toArray();
        // dd($data_edit_blog);
        return view('admin.Blog.editblog',compact("data_edit_blog"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddBlogRequest $request, $id)
    {
        $blog = blog::find($id);

        $data = $request->all();
        $file = $request->image;  

        if(!empty($file)){
            $data['image']=$file->getClientOriginalName();
        }
        
        if($blog->update($data)){
            if(!empty($file)){
                $file->move('upload/blog/image',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',__('Edit blog success.'));
        }
        else{
            return redirect()->back()->withErrors('Edit blog error.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
