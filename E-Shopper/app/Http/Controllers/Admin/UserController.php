<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Auth;
use App\Http\Requests\LoginRequest;
use App\Models\country;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_country = country::all()->toArray();
        // dd($data);
        return view('.admin.user.pages-profile', compact("data_country"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LoginRequest $Request)
    {
        $userID = Auth::id();
        $user = User::find($userID);

        $data = $Request->all();
        $file = $Request->avatar;  
 
        if(!empty($file)){
            $data['avatar']=$file->getClientOriginalName();
        }
        if($data['password']){
            $data['password']=bcrypt($data['password']);
        }
        else{
            $data['password']=$user->password;
        }
        
        if($user->update($data)){
            if(!empty($file)){
                $file->move('upload/user/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',__('Update profile success.'));
        }
        else{
            return redirect()->back()->withErrors('Update profile error.');
        }
        // if($request->hasFile('filesTest')){
        //     $file = $request->filesTest;
        //     echo 'Tên File:'.$file->getClientOriginalName();
        //     echo '<br/>';
        //     echo 'Đuôi File:'.$file->getRealPath();
        //     echo '<br/>';
        //     echo 'Kích cỡ File:'.$file->getSize();
        //     echo '<br/>';
        //     echo 'Kiểu Files: '.$file->getMimeType();
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
