<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\product;
use Illuminate\Support\Facades\DB; 

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_search(request $request)
    {
        $key_search = $request->name;
        // dd($key_search);
        $product = product::where('name','like','%'.$key_search.'%')
        ->get()->toArray();
        // dd($product);
        return view('frontend.search.search',compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search_advanced(request $request)
    {
        $product = product::query();
        // dd($product);
        $input = $request->all();
        // dd($input);
        if(!empty($input['name'])){
            $product->where('name','like','%'.$input['name'].'%');
        }
        if(!empty($input['price'])){
            $price_from = explode("-",$input['price']);
            $product->whereBetween('price',[$price_from[0],$price_from[1]]);
        }
        if(!empty($input['id_category'])){
            $product->where('id_category',$input['id_category']);        
        }
        if(!empty($input['id_brands'])){
            $product->where('id_brands',$input['id_brands']);
        }
        if(!empty($input['status'])){
            $product->where('status',$input['status']);
        }
        $data = $product->get()->toArray();
        // dd($data);
        return view('frontend.search.search_advanced',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search_price(Request $request)
    {
        $from = $request->get('from');
        // var_dump($from);
        $to = $request->get('to');
        // var_dump($to);
        $product = product::query();
        $data = $product->whereBetween('price',[$from,$to])->get()->toArray();
        // dd($data);
        return response()->json($data);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
