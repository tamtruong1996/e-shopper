<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Mail\MailNotify;
use App\Models\product;
use App\Models\history;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $data = [
    //         'subject' => 'Cambo Tutorial Mail',
    //         'body' => "Hello"
    //     ];
    //     try{
    //         Mail::to('truongthanhtam2131996@gmail.com.vn')->send(new MailNotify($data));
    //         return response()->json(['Great check your mail box']);
    //     } catch(Exception $th){
    //         return response()->json(['Sorry']);
    //     }
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function sendmail(request $request)
    {
        if(session()->has('data')){
            $data = Session::get('data');
            // dd($data);
            $name = $data['name'];
            $email =  $data['email'];
            $password =  Session::get('pass');
            $login = [
                'email' => $email,
                'password' => bcrypt($password),
            ];
            // dd($login);
            Auth::attempt($login);
        }
        else if(Auth::check()==true){
            $name = Auth::user()->name;
            // dd($name);
            $email= Auth::user()->email;
            // dd($email);
        }
        else{
            return redirect()->back()->with('error','Mua Hàng Không Thành Công');   
        }
        $session = session()->get('cart');
        // dd($session);   
        $total = 0;
        foreach($session as $val){
            $product = product::where('id',$val['id'])->get()->toArray();
            $product = $product[0];
            $cart[]= array_merge($product,$val);
            // dd($cart);
        }
        foreach($cart as $value){
            $total = $total + ($value['price']*$value['qty']);
            // dd($total);
        }
        $data = [
            'subject'=>'Đơn hàng',
            'body'=>$cart,
            'total'=>$total,
        ];
        // dd($data);
        try {
            Mail::to($email)->send(new Mailnotify($data));
            $user = User::where('email',$email)->get()->toArray();
            $user = $user[0];
            // dd($user);
            $history = new history();
            $history->email=$user['email'];
            $history->phone=$user['phone'];
            $history->name=$user['name'];
            $history->id_user=$user['id'];
            $history->price=($total + $total);
            $history->save();
            return response()->json('Mua hang thanh cong. Vui long kiem tra mail');
        } catch (Exception $th) {
            return redirect()->back()->with('error','Failed');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
