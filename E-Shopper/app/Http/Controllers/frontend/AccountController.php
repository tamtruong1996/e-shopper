<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Models\product;
use App\Models\brands;
use App\Models\category;
use Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.account.account');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_account(LoginRequest $request)
    {
        $userID = Auth::id();
        $user = User::find($userID);

        $data = $request->all();
        $data['level']=0;
        $file = $request->avatar;  
 
        if(!empty($file)){
            $data['avatar']=$file->getClientOriginalName();
        }
        if($data['password']){
            $data['password']=bcrypt($data['password']);
        }
        else{
            $data['password']=$user->password;
        }
        
        if($user->update($data)){
            if(!empty($file)){
                $file->move('upload/member/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',__('Update profile success.'));
        }
        else{
            return redirect()->back()->withErrors('Update profile error.');
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
