<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterMemberRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Models\product;
use App\Models\brands;
use App\Models\category;
use App\Models\country;
use Auth;
use Image;

class ProductController extends Controller
{
    public function index()
    {
        $id = Auth::id();
        $product = product::where('id_user',$id)->get()->toArray();
        // dd($product);
        return view('frontend.account.myproduct',compact('product'));
    }

    public function get_product()
    {
        $category = category::all()->toArray();
        // dd($category);
        $brands = brands::all()->toArray();
        // dd($brands);
        return view('frontend.account.addproduct',compact('category','brands'));
    }

    public function xulyhinhanh($request,$id)
    {
        if($request->hasfile('files'))
        {
            foreach($request->file('files') as $images)
            {
                $name = $images->getClientOriginalName();
                $name_2 = "IMG_85".$images->getClientOriginalName();
                $name_3 = "IMG_329".$images->getClientOriginalName();

                //$image->move('upload/product/', $name);
                
                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($images->getRealPath())->save($path);
                Image::make($images->getRealPath())->resize(85, 84)->save($path2);
                Image::make($images->getRealPath())->resize(329, 380)->save($path3);
                
                $data[] = $name;
            }
        }
    }

    public function add_product(Request $request)
    {
        if($request->hasfile('files'))
        {
            foreach($request->file('files') as $images)
            {
                $name = $images->getClientOriginalName();
                $name_2 = "IMG_85".$images->getClientOriginalName();
                $name_3 = "IMG_329".$images->getClientOriginalName();

                //$image->move('upload/product/', $name);
                
                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($images->getRealPath())->save($path);
                Image::make($images->getRealPath())->resize(85, 84)->save($path2);
                Image::make($images->getRealPath())->resize(329, 380)->save($path3);
                
                $data[] = $name;
            } 
        }    
        $product= new product();
        $product->id_brands = $request->id_brands; 
        $product->id_category = $request->id_category;
        $product->status = $request->status; 
        $product->name = $request->title; 
        $product->price = $request->price;
        $product->sale = $request->sale;
        $product->company = $request->company;
        $product->detail = $request->detail;
        $product['id_user']=Auth::id();
        $product->hinhanh=json_encode($data);
        $product->save();

        return redirect('/member/account/myproduct')->with('success', 'Your images has been successfully');
    }

    public function get_update_product($id)
    {
        $product = product::where('id',$id)->get()->toArray();
        $product = $product[0];
        $category = category::all();
        $brands = brands::all();
        return view('frontend.account.editproduct',compact('product','category','brands'));
    }

    public function post_update_product($id, Request $Request)
    {   
        // hinhxoa [1,2]
        // hinhcu[1,2,3]
        // foreach va in_Array
        // neu cõ thi xoa key trong arr (unset key in array php)
        // hinhconlai [3] => reset key => 0
        // => save BD => XONG
        // truong co upload mới:
        // hinhmoi count[4,5,6] + count(hinhconlai) > 3
        // else{
        //     xu ly hinh mới => đưa tất ca name hinh vao mang [4,5]
        //     merge [4,5] + [3] => DB
        // }
        $product = product::where('id',$id)->get()->toArray();
        $data = $Request->except('_token');

        $array_image_delete = 0;
        // hình ảnh cần xóa
        if(!empty($data['delete'])){
            $array_image_delete = count($data['delete']);
            // dd($array_image_delete);
        }

        // hình ảnh hiện tại
        $array_image_old = json_decode($product[0]['hinhanh'], true);
        // dd($array_image_old);

        $array_image_update = 0;
        // hình ảnh cần thêm mới
        if(!empty($data['files'])){
            $array_image_update = count($data['files']);
            // dd($array_image_update);
        }
        
        $sum = (count($array_image_old) + $array_image_update) - $array_image_delete;
        // dd($sum);
        if($sum < 1 || $sum > 3 ){
            return back()->with('error', 'số ảnh phải từ 1 đến 3');
        }
        else{
            if($array_image_delete!=0){
                // xóa hình ảnh cần xóa trong mảng hình ảnh hiện tại
                foreach($array_image_old as $key => $value){
                    if(in_array($value, $data['delete'])){
                        unset($array_image_old[$key]);  
                    }
                }
                // reset key mảng về 0
                $res = array_values($array_image_old);
                // dd($res);  
            }
            if($array_image_update!=0){
                foreach($data['files'] as $val){
                    $this->xulyhinhanh($Request,$id);
                    $name = $val->getClientOriginalName();
                    $images_update_new[]= $name;
                }
                // Merge ảnh còn lại và ảnh mới update
                $array_image_old = array_merge($array_image_old,$images_update_new);
                // dd($array_image_old);
            }
            $data['hinhanh']=json_encode($array_image_old);
            $id_user = Auth::id();
            $product = product::find($id_user);
            // dd($data);
            if($product->update($data)){
                return redirect('/member/account/myproduct')->with('success', 'Your images has been successfully');
            }
            else{
                return back()->with('Fail', 'Update thất bại');
            }
        }
    }

    // delete product
    public function delete_product($id)
    {
        product::destroy($id);
        return back()->with('success','Da delete');
    }
    
    // show index product theo ngay  moi nhat
    public function get_product_detail($id)
    {
        $product = product::where('id',$id)->get()->toArray();
        $product = $product[0];
        return view('frontend.account.product-details',compact('product'));
    }

    // ajax add to cart
    public function addtocart(Request $request)
    {
        $id = $request->get('id');
        // $qty = $request->get('qty');
        $product = product::where('id',$id)->get()->toArray();
        $product = $product[0];
        $name = $product['name'];
        $price = $product['price'];
        $image = $product['hinhanh'];

        $array = [];
        $array['id'] = $id;
        $array['qty'] = 1;  
        $array['name'] = $name;
        $array['price'] = $price;
        $array['hinhanh'] = $image;

        // $request->session()->flush();   
        if(session()->has('cart')){
            $getSession = session()->get('cart');
            // echo "<pre>";
            // var_dump($getSession);
            $flag = 1;
            foreach($getSession as $key => $value){
                if($id == $value['id']){
                    $getSession[$key]['qty'] += 1;
                    // echo "<pre>";
                    // var_dump($getSession[$key]['qty']);
                    session()->put('cart',$getSession);
                    $flag = 0;
                    break;
                }
            }
                if($flag == 1){
                    session()->push('cart',$array);
                }
        } else{
            session()->push('cart',$array);
        }   
        // $xx = session()->get('cart');
        // echo "<pre>";
        // var_dump($xx);
        return response()->json(['success'=>'Add product to your cart successfully.']);
    }   
    
    public function get_cart()
    {
        $getSession = session()->get('cart');
        $sum = 0;
        $cart_sub_total = 0;
        $data = [];
        if(!empty($getSession)){
            foreach($getSession as $val){
                $product = product::where('id',$val['id'])->get()->toArray();
                $product = $product[0];
                $sum = $sum + $val['qty'];  
                $data[]= array_merge($product,$val);
                // dd($data);
                // break;
            }
        }
        if (!empty($data)){
            foreach($data as $value){
                $cart_sub_total = $cart_sub_total + ($value['price']*$value['qty']);
                // echo $cart_sub_total;
            }  
        }
        return view('frontend.account.cart',compact('data','sum','cart_sub_total'));
    }

    public function ajax_cart(Request $request)
    {
        $id = $request->get('id');
        $qty = $request->get('qty');
        $getSession = session()->get('cart');
        if($qty>=1){
            // var_dump($getSession);
            foreach($getSession as $key => $value){
                $getSession[$key]['qty'] = $qty;
                session()->put('cart',$getSession);
            }
        }else{
            session()->forget('cart',$getSession);
        }
        // $x = session()->get('cart');
        return response()->json([$qty]);
    }

    public function checkout()
    {
        $getSession = session()->get('cart');
        // dd($getSession);
        $cart_sub_total = 0;
        $data = [];
        if(!empty($getSession)){
            foreach($getSession as $val){
                $product = product::where('id',$val['id'])->get()->toArray();
                $product = $product[0];
                $data[]= array_merge($product,$val);
                // dd($data);
                // break;
            }
        }
        if (!empty($data)){
            foreach($data as $value){
                $cart_sub_total = $cart_sub_total + ($value['price']*$value['qty']);
                // echo $cart_sub_total;
                // break;
            } 
        }
        $data_country = country::all()->toArray();
        return view('frontend.account.checkout',compact('data','cart_sub_total','data_country'));  
    }

    public function check_sendmail(RegisterMemberRequest $request)
    {
        $user = new User();
        $data = request()->except(['_token']);
        $data['level']=0;
        $password = $data['password'];
        $file = $request->avatar;
        if(!empty($file)){
            $data['avatar']=$file->getClientOriginalName();
        }

        if($data['password']){
            $data['password']=bcrypt($data['password']);
        }
    
        if($user->insert($data)){
            if(!empty($file)){
                $file->move('upload/member/avatar',$file->getClientOriginalName());
            }
            return redirect('/member/account/cart/sendmail')->with(['data' => $data,'password'=>$password]);
        }
        else{
            return redirect()->back()->withErrors('Register error.');
        }
        $user->save();
    }
}
