<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterMemberRequest;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\country;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_country = country::all()->toArray();
        return view('frontend.login.register',compact("data_country"));
    }

    public function register(RegisterMemberRequest $request)
    {
        $user = new User();
        $data = request()->except(['_token']);
        $data['level']=0;
        $file = $request->avatar;  
        if(!empty($file)){
            $data['avatar']=$file->getClientOriginalName();
        }

        if($data['password']){
            $data['password']=bcrypt($data['password']);
        }
    
        if($user->insert($data)){
            if(!empty($file)){
                $file->move('upload/member/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',__('Register success.'));
        }
        else{
            return redirect()->back()->withErrors('Register error.');
        }
        $user->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
