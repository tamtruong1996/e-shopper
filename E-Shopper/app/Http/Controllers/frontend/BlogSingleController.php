<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\blog;
use App\Models\User;
use App\Models\rate;
use App\Models\comment;
use Auth;

class BlogSingleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // hiển thị blog list
    public function index()
    {   
        $data = blog::join('users', 'blog.id_user', 'users.id')
        ->select('blog.id','blog.image','blog.created_at','title','description','name')
        ->paginate(3);
        // dd($data);
        return view('frontend.blog.blog',compact("data"));
    }

    // hiển thị blog list single
    public function showlist($id)
    {
        // lấy data blog theo id
        $data = blog::join('users', 'blog.id_user', 'users.id')
        ->select('blog.id','blog.image','blog.created_at','title','description','name','txtContent')
        ->where('blog.id',$id)->get();

        // lấy data tạo phân trang
        $data2 = blog::find($id);
        $previous = blog::where('id', '<', $data2->id)->max('id');
        $next = blog::where('id', '>', $data2->id)->min('id');  

        // lấy data xử lý rate (đánh giá)
        $data_rate = rate::where('id_blog',$id)->get()->toArray();
        // dd($data_rate);
        $tbc = 0;
        $dien_lam_tron = 0;
        foreach($data_rate as $val){
            $tbc = $tbc + $val['point'];
        }
        $dien_lam_tron = count($data_rate);
        $tbc = round($tbc/$dien_lam_tron);
        // echo $tbc;

        // lấy data comment
        $comment = comment::where('id_blog',$id)->get() ;
        // dd($comment);

        return view('frontend.blog.blog_single',compact("data","previous","next","tbc","dien_lam_tron","data_rate","comment"));
    }

    // tạo data comment
    public function comment(Request $request, $id){

        // dd($request->all());
        $id_user = Auth::id();
        $name = Auth()->user()->name;
        $avatar = Auth()->user()->avatar;

        $comment = new comment();
        $comment['id_user']=$id_user;
        $comment['id_blog']=$id;
        $comment['avatar']=$avatar;
        $comment['name']=$name;
        $comment['cmt']=$request->message;
        $comment['level']=$request->level;
        // dd($request->level);

        $comment->save();

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.   
     *
     * @return \Illuminate\Http\Response
     */
    // tạo data rate
    public function ajax_rate(Request $request)
    {
        $id_user = Auth::id();
        $id_blog = $request->get('id_blog');
        $values = $request->get('values');

        $rate = new rate();
        $rate['point']=$values;
        $rate['id_user']=$id_user;
        $rate['id_blog']=$id_blog;
        $rate->save(); 

        return $values;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
