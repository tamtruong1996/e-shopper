<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'image' =>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'description' => 'required',
            'txtContent' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute Không được để trống',
            'image' => ':attribute vừa nhập không phải dạng hình ảnh',
            'mimes' => ':attribute phải thuộc các định dạng sau : jpeg,png,jpg,gif',
        ];
    }
    public function attributes()
    {
        return [
            'title' => 'Tiêu đề',
            'image' => 'Hình ảnh',
            'description' => 'Nội dung',
            'txtContent' => 'Content',
        ];
    }
}
