<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'email' => 'required',
            'password' =>'required|max:20',
            'avatar' =>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'address' =>'required',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute Không được để trống',
            'max' => ':attribute Không được quá :max ký tự',
            'image' => ':attribute vừa nhập không phải dạng hình ảnh',
            'mimes' => ':attribute phải thuộc các định dạng sau : jpeg,png,jpg,gif',
        ];
    }
    public function attributes()
    {
        return [
            'name' => 'Tên người dùng',
            'email' => 'Địa chỉ mail',
            'password' => 'Mật khẩu',
            'avatar' => 'Ảnh đại diện',
            'address' => 'Đại chỉ',
        ];
    }
}
