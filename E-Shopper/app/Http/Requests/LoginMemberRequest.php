<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' =>'required|max:20',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute Không được để trống',
            'max' => ':attribute Không được quá :max ký tự',
        ];
    }
    public function attributes()
    {
        return [
            'email' => 'Địa chỉ mail',
            'password' => 'Mật khẩu',
        ];
    }
}
